import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeftSideNavComponent } from './left-side-nav.component';



@NgModule({
  declarations: [LeftSideNavComponent],
  imports: [
    CommonModule
  ],
  exports: [LeftSideNavComponent]
})
export class LeftSideNavModule { }
