import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { LeftSideNavModule } from '../shared/modules/left-side-nav/left-side-nav.module';

@NgModule({
  declarations: [PagesComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    LeftSideNavModule
  ]
})
export class PagesModule { }
